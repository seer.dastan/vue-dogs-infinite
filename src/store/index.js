import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import localStoragePlugin, { SAVE_NAMES } from './plugins/localStorage'
Vue.use(Vuex)

export const ADD_DOGS = 'ADD_DOGS'
export const ADD_BREEDS = 'ADD_BREEDS'
export const TOGGLE_FAVOURITE = 'TOGGLE_FAVOURITE'
/** Превращение src в object с именем */
export function srcToObject (src) {
  const regex = /(?<=\/breeds\/)(.*)(?=\/)/gm
  const name = src.match(regex)[0]
  return {
    src,
    name
  }
}

const state = {
  dogs: [],
  breeds: [],
  favourites: []
}
Object.keys(SAVE_NAMES).forEach(mutationType => {
  const options = SAVE_NAMES[mutationType]

  if (localStorage.getItem(options.storageKey)) {
    const savedData = JSON.parse(localStorage.getItem(options.storageKey))
    if (savedData != null) {
      state[options.storeKey] = savedData
    }
  }
})
export default new Vuex.Store({
  plugins: [localStoragePlugin],
  state,
  getters: {
    getDogs: (state) => state.dogs,
    getFavourties: (state) => state.favourites,
    getBreeds: (state) => state.breeds,
    isInFavourite: state => src => state.favourites.includes(src)
  },
  mutations: {
    [ADD_DOGS] (state, dogs) {
      state.dogs = state.dogs.concat(dogs)
    },
    CLEAR_DOGS (state) {
      state.dogs = []
    },
    [ADD_BREEDS] (state, breeds) {
      state.breeds = state.breeds.concat(breeds)
    },
    [TOGGLE_FAVOURITE] (state, src) {
      const index = state.favourites.indexOf(src)
      if (index >= 0) {
        state.favourites.splice(index, 1)
      } else {
        state.favourites.push(src)
      }
    }
  },
  actions: {
    async fetchDogs ({ commit }, { breed }) {
      let url
      if (breed) {
        url = `https://dog.ceo/api/breed/${breed}/images/random/20`
      } else {
        url = 'https://dog.ceo/api/breeds/image/random/20'
      }

      const { data } = await axios.get(url)
      if (data.status === 'success') {
        const items = data.message.map(srcToObject)
        commit(ADD_DOGS, items)
      }
    },

    async fetchBreeds ({ commit }) {
      const url = 'https://dog.ceo/api/breeds/list/all'
      const { data } = await axios.get(url)
      if (data.status === 'success') {
        const breeds = []
        Object.keys(data.message).forEach(key => {
          /* if (data.message[key].length) {
            const newBreeds = data.message[key].map(country => `${key} ${country}`)
            breeds = [...breeds, ...newBreeds]
          } else { */
          breeds.push(key)
          // }
        })
        commit(ADD_BREEDS, breeds)
      }
    }

  }
})
