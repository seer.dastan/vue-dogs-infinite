export const SAVE_NAMES = {
  TOGGLE_FAVOURITE: {
    storageKey: 'app_favourites',
    storeKey: 'favourites'
  }
}
const localStoreagePlugin = store => {
  store.subscribe((mutation, state) => {
    if (typeof SAVE_NAMES[mutation.type] === 'object') {
      const options = SAVE_NAMES[mutation.type]
      localStorage.setItem(options.storageKey, JSON.stringify(state[options.storeKey]))
    }
  })
}

export default localStoreagePlugin
